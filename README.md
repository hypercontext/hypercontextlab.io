Site Statique du projet HyperContext
=======================================

Ceci est le code source du site de projet HyperContext. Ce projet vise a développer et promouvoir un ensemble de dispositifs technologiques en vue de permettre une Lecture Participative de contenus en ligne.

Le site généré à chaque modification est visible sur https://hypercontext.gitlab.io 

Installation
-------------

Partons du principe que vous avez déjà [Git][git] installé sur votre ordinateur, que vous avez un compte gitlab et que vous avez déjà [ajouté votre clé publique SSH dans votre compte gitlab][ssh-gitlab].

Soit vous avez droit d'écriture sur ce repo ici présent, soit vous pouvez le forker pour faire ensuite une Merge Request (en français: une demande de fusion), à savoir une sorte de proposition de modification que ceux qui ont accès en écriture sur ce repo pourront accepter.

Soit vous avez Docker sur votre machine:

    git clone git@gitlab.com:refrac/hypercontext.gitlab.io.git
    cd hypercontext.gitlab.io
    git submodule update --init
    docker-compose up
    open http://localhost:1313/

Soit sans docker, il faut d'abord [installer Hugo][install], puis

    git clone git@gitlab.com:refrac/refrac.gitlab.io.git
    cd hypercontext.gitlab.io
    git submodule update --init
    hugo server -b http://localhost:1313/
    open http://localhost:1313/

Sans installation
--------------------

Vous pouvez aussi éviter de vous embrouiller la tête et utiliser l'interface de Gitlab pour éditer ou ajouter des fichiers. Il suffit d'aller dans le dossier `content/` et de trouver la page à modifier ou l'endroit ou en ajouter une, et de cliquer sur le bouton `Edit`. Au moment de sauver, il proposera soit de faire une Merge Request soit de sauver directement si vous en avez le droit.

Conventions d'écriture
------------------------

Hugo utilise le [Markdown][md] pour les fichiers de contenu. C'est un système de marquage simple et lisible, très facile à prendre en main.

Contributeurs
----------------

Le projet HyperContext est porté par:

* Franck Cormerais (méthodologie)
* mose (technologie)

Pour toute demande d'aide au sujet de tout ce dispositif, demandez à mose.

Copyright
------------

Au diable le Copyright, tout ce qui se trouve ici est Copyleft. Vive le domaine public. Si ca vous pose un problème, ne contribuez pas à ce projet.

![Domaine public](https://licensebuttons.net/p/zero/1.0/88x31.png)


[install]: https://gohugo.io/overview/installing/
[git]: https://git-scm.com/
[ssh-gitlab]: https://docs.gitlab.com/ee/ssh/
[md]: https://daringfireball.net/projects/markdown/