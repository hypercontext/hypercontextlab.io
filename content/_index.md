---
title: HyperConText
type: docs
---

# HyperConText

Le projet Hypercontext vise à donner corps une vision séculaire d'interaction avec l'univers numérique et les documents en ligne, en proposant des protocoles, des dispositifs et des solutions logicielles pour permettre une lecture participative de contenus en ligne.

C'est un projet a multiples étages, et divers champs d'application. Le premier champs d'application est académique et vise a ouvrir à l'annotation collaborative le corpus des œuvres de Bernard Stiegler (ce projet a pour nom de code HyperStiegler).

## Hypertexte et hypercontexte

La création du [web][1] en 1992 a donné lieu a la mise en visibilité du système d'[hypertexte][2] qui datait de 1965. Ce système permet a l'auteur d'une ressource écrite sur internet d'inclure dans son texte des liens vers d'autres ressources sur internet. Ce qui nous semble aujourd'hui naturel était en fait une petite révolution avec la rupture de la linéarité des structures textuelles, et ouvrait le champs du savoir à des horizons radieux dans le cyberespace.

L'hypercontexte se place dans cette égale perspective de briser a nouveau le mode navigationnel des textes en ligne, en donnant au lecteur cette fois-ci, et non plus a l'auteur, la possibilité technique de de-linéariser le texte qu'il lit, et ce faisant en rendant sa **lecture participative**.

Cette démarche vise a proposer une démarche d'intelligence collective dans un travail en profondeur sur des ressources en ligne, à l'heure ou on déplore de plus en plus la superficialisation de l'usage de l'internet. Les horizons radieux des débuts de l'internet ont assez vite fait place à l'emprise des principes de consommation informationelle (qui en bout de course nous a mené dans le monde de la post-réalité), et tout effort pour contrecarrer cette tendance nous semble digne d'y consacrer des efforts.



[1]: https://fr.wikipedia.org/wiki/World_Wide_Web
[2]: https://fr.wikipedia.org/wiki/Hypertexte