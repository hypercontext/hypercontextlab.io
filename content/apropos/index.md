---
title: A propos
---

Ce site est maintenu par un groupe de travail multi-disciplinaire, hébergé chez [Gitlab](https://gitlab.com/refrac/refrac.gitlab.io). Il est généré par [Hugo](https://gohugo.io) et utilise le thème [hugo-book](https://themes.gohugo.io/hugo-book/).

